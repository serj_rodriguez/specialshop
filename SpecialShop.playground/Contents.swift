import UIKit

//Problem
//Creatnx now wants to decorate his house by flower pots. He plans to buy exactly  ones. He can only buy them from Triracle's shop. There are only two kind of flower pots available in that shop. The shop is very strange. If you buy  flower pots of kind 1 then you must pay  and  if you buy  flower pots of kind 2. Please help Creatnx buys exactly  flower pots that minimizes money he pays.
//
//Input Format
//
//The first line contains a integer  denoting the number of test cases.
//
//Each of test case is described in a single line containing three space-separated integers .
//
//Output Format
//
//For each test case, print a single line containing the answer.
//
//Constraints
//
//
//
//Sample Input
//2
//5 1 2
//10 2 4
//
//Sample Output
//17
//134
//Time Limit: 1
//Memory Limit: 256
//Source Limit:
//Explanation
//Query 1: we have to buy exactly  pots. There are six possible options:
//
//Buy  pot of first kind,  pots of second kind. The cost is: 1x02 + 2X 52 = 50.
//Buy  pot of first kind,  pots of second kind. The cost is: .
//Buy  pots of first kind,  pots of second kind. The cost is: .
//Buy  pots of first kind,  pots of second kind. The cost is: .
//Buy  pots of first kind,  pot of second kind. The cost is: .
//Buy  pots of first kind,  pot of second kind. The cost is: .
//So, the optimal cost is 17.

func calculateBestPrice(_ numberOfFlowers: Int, _ pricePotA: Int, _ pricePotB: Int){
    
    var arr = [Int]()
    let range = 0...numberOfFlowers
    for currentNumberOfPotsToPrice in range{
        let currentNumberOfPotsB = range.upperBound - currentNumberOfPotsToPrice
        let calculatedPrice = pricePotA * (currentNumberOfPotsToPrice * currentNumberOfPotsToPrice) + pricePotB * (currentNumberOfPotsB * currentNumberOfPotsB)
        arr.append(calculatedPrice)
    }
    arr.sort()
    print(arr.first ?? 0)
}

calculateBestPrice(5, 1, 2)
calculateBestPrice(10, 2, 4)
calculateBestPrice(5, 5, 5)
